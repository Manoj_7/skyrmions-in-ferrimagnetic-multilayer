//mumax 3.10 [linux_amd64 go1.14(gc) CUDA-11.0]
//GPU info: NVIDIA A30(24062MB), CUDA Driver 12.2, cc=8.0, using cc=80 PTX
//(c) Arne Vansteenkiste, Dynamat LAB, Ghent University, Belgium
//This is free software without any warranty. See license.txt
//********************************************************************//
//  If you use mumax in any work or publication,                      //
//  we kindly ask you to cite the references in references.bib        //
//********************************************************************//
//output directory: AwrtB_C2-17.out/
t_m := 0.9e-9
t_r := 2.9e-9
n := 5
f := t_m / t_r
setgridsize(256, 256, 1)
setcellsize(3.125e-9, 3.125e-9, 15*0.9e-9)
setgeom(cuboid(800e-9, 800e-9, 13.5e-9))
SetPBC(2, 2, 0)
//resizing...
MsatCo := 1136538.462 * f
A_ex_num := 17e-12 * f
Keff := 68021.76923 * f
Kdemag := 0.5 * mu0 * MsatCo * MsatCo
Ku1value := Keff + Kdemag
DMI := 1.62e-3 * f
Msat = MsatCo
Aex = A_ex_num
alpha = 0.5
Ku1 = Ku1value
AnisU = vector(0, 0, 1)
Dind = DMI
B_min := -200e-3
B_max := 200e-3
B_step := 10e-3
MinimizerStop = 1e-5
tableadd(B_ext)
tableadd(E_demag)
tableadd(E_exch)
tableadd(E_anis)
tableadd(E_zeeman)
tableadd(E_total)
tableadd(ext_topologicalcharge)
tableadd(ext_topologicalchargedensity)
i := 0
simname := "m"
for B := 0.0; B <= B_max; B += B_step {
	print("B = ", B)
	m = randommag()
	B_ext = vector(0, 0.034899496*B, 0.999390827*B)
	relax()
	snapshot(m)
	MFMLift = 20e-9
	snapshot(MFM)
	simname = sprintf("m-%03d", i)
	tablesave()
	saveas(m, simname)
	i += 1
}
//B =  0
//B =  0.01
//B =  0.02
//B =  0.03
//B =  0.04
//B =  0.05
//B =  0.060000000000000005
//B =  0.07
//B =  0.08
//B =  0.09
//B =  0.09999999999999999
//B =  0.10999999999999999
//B =  0.11999999999999998
//B =  0.12999999999999998
//B =  0.13999999999999999
//B =  0.15
//B =  0.16
//B =  0.17
//B =  0.18000000000000002
//B =  0.19000000000000003
//********************************************************************//
//Please cite the following references, relevant for your simulation. //
//See bibtex file in output folder for justification.                 //
//********************************************************************//
//   * Mulkers et al., Phys. Rev. B 95, 144401 (2017).
//   * Vansteenkiste et al., AIP Adv. 4, 107133 (2014).

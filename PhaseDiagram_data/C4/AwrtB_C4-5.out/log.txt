//mumax 3.10 [linux_amd64 go1.14(gc) CUDA-11.0]
//GPU info: NVIDIA A30(24062MB), CUDA Driver 12.2, cc=8.0, using cc=80 PTX
//(c) Arne Vansteenkiste, Dynamat LAB, Ghent University, Belgium
//This is free software without any warranty. See license.txt
//********************************************************************//
//  If you use mumax in any work or publication,                      //
//  we kindly ask you to cite the references in references.bib        //
//********************************************************************//
//output directory: AwrtB_C4-5.out/
t_m := 0.9e-9
t_r := 2.9e-9
n := 5
f := t_m / t_r
setgridsize(256, 256, 1)
setcellsize(3.125e-9, 3.125e-9, 15*0.9e-9)
setgeom(cuboid(800e-9, 800e-9, 13.5e-9))
SetPBC(2, 2, 0)
//resizing...
MsatCo := 788942.3077 * f
A_ex_num := 5e-12 * f
Keff := 62573.30769 * f
Kdemag := 0.5 * mu0 * MsatCo * MsatCo
Ku1value := Keff + Kdemag
DMI := 8.88e-4 * f
Msat = MsatCo
Aex = A_ex_num
alpha = 0.5
Ku1 = Ku1value
AnisU = vector(0, 0, 1)
Dind = DMI
B_min := -150e-3
B_max := 150e-3
B_step := 5e-3
MinimizerStop = 1e-5
tableadd(B_ext)
tableadd(E_demag)
tableadd(E_exch)
tableadd(E_anis)
tableadd(E_zeeman)
tableadd(E_total)
tableadd(ext_topologicalcharge)
tableadd(ext_topologicalchargedensity)
i := 0
simname := "m"
for B := 0.0; B <= B_max; B += B_step {
	print("B = ", B)
	m = randommag()
	B_ext = vector(0, 0.034899496*B, 0.999390827*B)
	relax()
	snapshot(m)
	MFMLift = 20e-9
	snapshot(MFM)
	simname = sprintf("m-%03d", i)
	tablesave()
	saveas(m, simname)
	i += 1
}
//B =  0
//B =  0.005
//B =  0.01
//B =  0.015
//B =  0.02
//B =  0.025
//B =  0.030000000000000002
//B =  0.035
//B =  0.04
//B =  0.045
//B =  0.049999999999999996
//B =  0.05499999999999999
//B =  0.05999999999999999
//B =  0.06499999999999999
//B =  0.06999999999999999
//B =  0.075
//B =  0.08
//B =  0.085
//B =  0.09000000000000001
//B =  0.09500000000000001
//B =  0.10000000000000002
//B =  0.10500000000000002
//B =  0.11000000000000003
//B =  0.11500000000000003
//B =  0.12000000000000004
//B =  0.12500000000000003
//B =  0.13000000000000003
//B =  0.13500000000000004
//B =  0.14000000000000004
//B =  0.14500000000000005
//********************************************************************//
//Please cite the following references, relevant for your simulation. //
//See bibtex file in output folder for justification.                 //
//********************************************************************//
//   * Vansteenkiste et al., AIP Adv. 4, 107133 (2014).
//   * Mulkers et al., Phys. Rev. B 95, 144401 (2017).

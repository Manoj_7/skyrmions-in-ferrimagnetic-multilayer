//mumax 3.10 [linux_amd64 go1.14(gc) CUDA-11.0]
//GPU info: NVIDIA GeForce GTX 1050 Ti(4040MB), CUDA Driver 12.2, cc=6.1, using cc=61 PTX
//(c) Arne Vansteenkiste, Dynamat LAB, Ghent University, Belgium
//This is free software without any warranty. See license.txt
//********************************************************************//
//  If you use mumax in any work or publication,                      //
//  we kindly ask you to cite the references in references.bib        //
//********************************************************************//
//output directory: 4zeroed_starting_state.out/
t_m := 0.9e-9
t_r := 2.9e-9
n := 5
f := t_m / t_r
setgridsize(256, 256, 1)
setcellsize(3.125e-9, 3.125e-9, 15*0.9e-9)
setgeom(cuboid(800e-9, 800e-9, 13.5e-9))
SetPBC(0, 0, 0)
//resizing...
MsatCo := 788942.3077 * f
A_ex_num := 5e-12 * f
Keff := 62573.30769 * f
Kdemag := 0.5 * mu0 * MsatCo * MsatCo
Ku1value := Keff + Kdemag
DMI := 8.88e-4 * f
Msat = MsatCo
Aex = A_ex_num
alpha = 0.5
Ku1 = Ku1value
AnisU = vector(0, 0, 1)
Dind = DMI
tableadd(B_ext)
tableadd(E_demag)
tableadd(E_exch)
tableadd(E_anis)
tableadd(E_zeeman)
tableadd(E_total)
m = randommag()
B_ext = vector(0, 0, 0)
relax()
snapshot(m)
saveas(m, sprint("Zeroed_Starting_State_4"))
//********************************************************************//
//Please cite the following references, relevant for your simulation. //
//See bibtex file in output folder for justification.                 //
//********************************************************************//
//   * Vansteenkiste et al., AIP Adv. 4, 107133 (2014).
//   * Mulkers et al., Phys. Rev. B 95, 144401 (2017).

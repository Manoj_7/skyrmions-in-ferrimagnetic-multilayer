//mumax 3.10 [linux_amd64 go1.14(gc) CUDA-10.1]
//GPU info: Tesla T4(15102MB), CUDA Driver 12.2, cc=7.5, using cc=75 PTX
//(c) Arne Vansteenkiste, Dynamat LAB, Ghent University, Belgium
//This is free software without any warranty. See license.txt
//********************************************************************//
//  If you use mumax in any work or publication,                      //
//  we kindly ask you to cite the references in references.bib        //
//********************************************************************//
//output directory: sample3_hysteresis.out/
t_m := 0.9e-9
t_r := 2.9e-9
n := 5
f := t_m / t_r
setgridsize(256, 256, 1)
setcellsize(3.125e-9, 3.125e-9, 15*0.9e-9)
setgeom(cuboid(800e-9, 800e-9, 13.5e-9))
SetPBC(0, 0, 0)
//resizing...
MsatCo := 984519.2308 * f
A_ex_num := 9e-12 * f
Keff := 66703.84615 * f
Kdemag := 0.5 * mu0 * MsatCo * MsatCo
Ku1value := Keff + Kdemag
DMI := 1.27e-3 * f
Msat = MsatCo
Aex = A_ex_num
alpha = 0.5
Ku1 = Ku1value
AnisU = vector(0, 0, 1)
Dind = DMI
B_min := -150e-3
B_max := 150e-3
B_step := 5e-3
MinimizerStop = 1e-5
tableadd(B_ext)
tableadd(E_demag)
tableadd(E_exch)
tableadd(E_anis)
tableadd(E_zeeman)
tableadd(E_total)
tableadd(ext_topologicalcharge)
tableadd(ext_topologicalchargedensity)
i := 0
simname := "m"
for B := B_max; B >= -B_max; B -= B_step {
	print("B = ", B)
	m = randommag()
	B_ext = vector(0, 0.034899496*B, 0.999390827*B)
	relax()
	snapshot(m)
	MFMLift = 20e-9
	snapshot(MFM)
	simname = sprintf("m-%03d", i)
	tablesave()
	saveas(m, simname)
	i += 1
}
//B =  0.15
//B =  0.145
//B =  0.13999999999999999
//B =  0.13499999999999998
//B =  0.12999999999999998
//B =  0.12499999999999997
//B =  0.11999999999999997
//B =  0.11499999999999996
//B =  0.10999999999999996
//B =  0.10499999999999995
//B =  0.09999999999999995
//B =  0.09499999999999995
//B =  0.08999999999999994
//B =  0.08499999999999994
//B =  0.07999999999999993
//B =  0.07499999999999993
//B =  0.06999999999999992
//B =  0.06499999999999992
//B =  0.05999999999999992
//B =  0.054999999999999924
//B =  0.049999999999999926
//B =  0.04499999999999993
//B =  0.03999999999999993
//B =  0.034999999999999934
//B =  0.029999999999999933
//B =  0.024999999999999932
//B =  0.01999999999999993
//B =  0.01499999999999993
//B =  0.009999999999999929
//B =  0.004999999999999929
//B =  -7.112366251504909e-17
//B =  -0.005000000000000071
//B =  -0.010000000000000071
//B =  -0.015000000000000072
//B =  -0.020000000000000073
//B =  -0.025000000000000074
//B =  -0.030000000000000075
//B =  -0.03500000000000007
//B =  -0.04000000000000007
//B =  -0.04500000000000007
//B =  -0.050000000000000065
//B =  -0.05500000000000006
//B =  -0.06000000000000006
//B =  -0.06500000000000006
//B =  -0.07000000000000006
//B =  -0.07500000000000007
//B =  -0.08000000000000007
//B =  -0.08500000000000008
//B =  -0.09000000000000008
//B =  -0.09500000000000008
//B =  -0.10000000000000009
//B =  -0.1050000000000001
//B =  -0.1100000000000001
//B =  -0.1150000000000001
//B =  -0.1200000000000001
//B =  -0.1250000000000001
//B =  -0.13000000000000012
//B =  -0.13500000000000012
//B =  -0.14000000000000012
//B =  -0.14500000000000013
//********************************************************************//
//Please cite the following references, relevant for your simulation. //
//See bibtex file in output folder for justification.                 //
//********************************************************************//
//   * Mulkers et al., Phys. Rev. B 95, 144401 (2017).
//   * Vansteenkiste et al., AIP Adv. 4, 107133 (2014).

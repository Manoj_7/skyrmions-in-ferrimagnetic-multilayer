//mumax 3.10 [linux_amd64 go1.14(gc) CUDA-10.1]
//GPU info: Tesla T4(15102MB), CUDA Driver 12.2, cc=7.5, using cc=75 PTX
//(c) Arne Vansteenkiste, Dynamat LAB, Ghent University, Belgium
//This is free software without any warranty. See license.txt
//********************************************************************//
//  If you use mumax in any work or publication,                      //
//  we kindly ask you to cite the references in references.bib        //
//********************************************************************//
//output directory: S6_A3_5.out/
t_m := 0.9e-9
t_r := 2.9e-9
n := 5
f := t_m / t_r
setgridsize(256, 256, 1)
setcellsize(3.125e-9, 3.125e-9, 15*0.9e-9)
setgeom(cuboid(800e-9, 800e-9, 13.5e-9))
SetPBC(0, 0, 0)
//resizing...
MsatCo := 599423.0769 * f
A_ex_num := 3.5e-12 * f
Keff := 44760.57692 * f
Kdemag := 0.5 * mu0 * MsatCo * MsatCo
Ku1value := Keff + Kdemag
DMI := 5.76e-4 * f
Msat = MsatCo
Aex = A_ex_num
alpha = 0.5
Ku1 = Ku1value
AnisU = vector(0, 0, 1)
Dind = DMI
tableadd(Temp)
tableadd(B_ext)
tableadd(E_demag)
tableadd(E_exch)
tableadd(E_anis)
tableadd(E_zeeman)
tableadd(E_total)
m = randommag()
B_ext = vector(0, 0, 53e-3)
relax()
tablesave()
saveas(m, sprint("final_relaxed_ground_state"))
//********************************************************************//
//Please cite the following references, relevant for your simulation. //
//See bibtex file in output folder for justification.                 //
//********************************************************************//
//   * Mulkers et al., Phys. Rev. B 95, 144401 (2017).
//   * Vansteenkiste et al., AIP Adv. 4, 107133 (2014).
